const db = require('../db');

async function getCity(minimum_population) {
  // Box around metropolitan France
  const box = {
    lat1: 41.294,
    lng1: -7.163,
    lat2: 51.727,
    lng2: 10.767
  }
  const [results, metadata] = await db.query('SELECT ville_nom_reel as name, ville_latitude_deg as lat, ville_longitude_deg as lng FROM villes_france_free WHERE ville_population_2012 >= ? AND ville_latitude_deg BETWEEN ? AND ? AND ville_longitude_deg BETWEEN ? AND ? ORDER BY RAND() LIMIT 1',
    {
      replacements: [minimum_population,
        box.lat1, box.lat2, box.lng1, box.lng2]
    });
  const { name, lat, lng }= results[0];
  return { name, lat, lng };
}

module.exports = {
  getCity,
};
