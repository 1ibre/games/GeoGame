require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const session = require('express-session');
const csrf = require('csurf');
const redis = require('redis');
const redisClient = redis.createClient({
  url: process.env.REDIS_URI || 'redis://localhost:6379/0',
  legacyMode: true
});
const redisStore = require('connect-redis')(session);

redisClient.on('error', (err) => {
  console.error('Redis errorr', err);
});

(async () => {
  await redisClient.connect();
  console.log('Redis connected');
})();

const app = express();

app.use(session({
  secret: process.env.SESSION_SECRET,
  name: '_redisPractice',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }, // Note that the cookie-parser module is no longer needed
  store: new redisStore(
    {
      client: redisClient,
      ttl: 86400
    }
  ),
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const apiRouter = require('./routes/api');
const csrfProtection = csrf({ cookie: true });
const indexRouter = require('./routes/index');
const authRouter = require('./routes/auth');

app.use('/api/0', apiRouter);
app.use(csrfProtection);
app.use('/', indexRouter);
app.use('/auth', authRouter);

// Add nodemodules dist lib
app.use('/dist/jquery', express.static(path.join(__dirname, 'node_modules/jquery/dist')));
app.use('/dist/axios', express.static(path.join(__dirname, 'node_modules/axios/dist')));
app.use('/dist/leaflet', express.static(path.join(__dirname, 'node_modules/leaflet/dist')));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
