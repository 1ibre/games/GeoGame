const express = require('express');
const router = express.Router();
const controller = require('../controllers/index');

router.route('/')
    .get(controller.index);

router.route('/game')
    .get(controller.game);

router.route('/about')
    .get(controller.about);

module.exports = router;
