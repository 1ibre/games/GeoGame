const debug = require('debug')('geogame:api');
const cities = require('../models/city');
const MINIMUM_POPULATION = process.env.MINIMUM_POPULATION || 50000;

function docs(req, res) {
  res.render('api/docs');
}

async function getCity(req, res) {
  if (req.session.username === undefined) {
    res.status(401).send('Unauthorized');
    return;
  }
  const city = await cities.getCity(MINIMUM_POPULATION);
  debug('city:', city);
  req.session.city = city;
  res.json(city);
}

function checkCoordinates(req, res) {
  const { lat, lng, city } = req.body;
  debug('Coords check: ', lat, lng, city);
  try {
    if (lat === undefined || lng === undefined || city === undefined) {
      debug("Missing parameters");
      res.status(400).send('Bad Request');
      return;
    }
    if (req.session.city.name == undefined || req.session.city.name !== city) {
      debug("City name doesn't match");
      res.status(406).send('Not Acceptable');
      return;
    }
    debug("Computing distance");
    const [real_lat, real_lng] = [req.session.city.lat, req.session.city.lng];
    let dist = distance(lat, lng, real_lat, real_lng);
    debug("Distance: ", dist);
    res.json({ dist, coords: { lat: real_lat, lng: real_lng } });
  } catch (error) {
    debug(error);
    res.status(500).send('Internal Server Error');
  }
}

function radians(deg) {
  return deg * Math.PI / 180;
}

function distance(lat1, lng1, lat2, lng2) {
  let r = 6371;
  let d = (2 * r) *
    Math.asin(Math.sqrt(0.5 - Math.cos(radians(lat1 - lat2)) / 2 + Math.cos(radians(lat2)) * Math.cos(radians(lat1)) *
      (1 - Math.cos(radians(lng1 - lng2))) / 2))
  return d;
}

module.exports = {
  docs,
  getCity,
  checkCoordinates
}
