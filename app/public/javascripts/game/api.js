const csrfToken = $('meta[name="csrf-token"]').attr('content');

const api = axios.create({
  baseURL: '/api/0',
  timeout: 5000,
  headers: {
    'X-CSRF-Token': csrfToken,
  }
});

async function getCity() {
  return await api.get('/city/random');
}

async function checkCoords(coords, city) {
  return await api.post('/city/check', { lat: coords[0], lng: coords[1], city });
}

const API = {
  getCity,
  checkCoords,
}

export default API;
