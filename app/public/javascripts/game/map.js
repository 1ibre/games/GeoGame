let marker,
  map,
  city,
  circle;

function clear() {
  if (marker != undefined) {
    map.removeLayer(marker);
    marker.placed = false;
  }
  if (circle != undefined) {
    map.removeLayer(circle);
  }
  if (city != undefined) {
    map.removeLayer(city);
  }
}

function init() {
  map = L.map('map').setView([46.958, 2.966], 5);

  let osm = L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"),
    sat = L.tileLayer("https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}", { subdomains: ['otile1', 'otile2', 'otile3', 'otile4'] });

  marker = L.marker([46.958, 2.966]);
  marker.placed = false;

  city = L.marker([46.958, 2.966]);

  let baseMaps = {
    // "OpenStreetMap": osm
    "Sattelite": sat
  };

  let overlays = {//add any overlays here

  };

  // L.control.layers(baseMaps, overlays, { position: 'bottomleft' }).addTo(map);
  sat.addTo(map);

  map.on('click', function (e) {
    if (!marker.placed) {
      marker.addTo(map);
      marker.placed = true;
      marker.setLatLng(e.latlng);
    }
  });
}

function setCircle(coords, dist) {
  if (circle != undefined) {
    map.removeLayer(circle);
  }
  circle = L.circle([coords.lat, coords.lng], {
    color: 'grey',
    fillColor: '#fffff',
    fillOpacity: 0.25,
    radius: dist * 1000
  }).addTo(map);
  if (city != undefined) {
    map.removeLayer(city);
  }
  city.setLatLng([coords.lat, coords.lng]);
  city.addTo(map);
  city._icon.style.filter = "hue-rotate(120deg)";
}

function getCoords() {
  return marker.getLatLng();
}

init();

const Map = {
  map,
  marker,
  init,
  clear,
  getCoords,
  setCircle
}

export default Map;
